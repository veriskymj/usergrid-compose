#!/bin/bash

# Copyright 2014-2015 Jahn Bertsch
# Copyright 2015 TOMORROW FOCUS News+ GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# this script is invoked after starting up the docker container.
# it allows for configuration at run time instead of baking all
# configuration settings into the container. you can set all configurable
# options using environment variables.
#
# overwrite any of the following default values at run-time like this:
#  docker run --env <key>=<value>
if [ -z "${ADMIN_USER}" ]; then
  ADMIN_USER=admin
fi
if [ -z "${ADMIN_PASS}" ]; then
  ADMIN_PASS=admin
fi
if [ -z "${ORG_NAME}" ]; then
  ORG_NAME=org
fi
if [ -z "${APP_NAME}" ]; then
  APP_NAME=app
fi
if [ -z "${TOMCAT_RAM}" ]; then
  TOMCAT_RAM=512m
fi

echo "+++ usergrid configuration:  CASSANDRA_CLUSTER_NAME=${CASSANDRA_CLUSTER_NAME}  USERGRID_CLUSTER_NAME=${USERGRID_CLUSTER_NAME}  ADMIN_USER=${ADMIN_USER}  ORG_NAME=${ORG_NAME}  APP_NAME=${APP_NAME}  TOMCAT_RAM=${TOMCAT_RAM}"


# start usergrid
# ==============

# update tomcat's java options
sed -i "s#\"-Djava.awt.headless=true -Xmx128m -XX:+UseConcMarkSweepGC\"#\"-Djava.awt.headless=true -XX:+UseConcMarkSweepGC -Xmx${TOMCAT_RAM} -Xms${TOMCAT_RAM} -verbose:gc\"#g" /etc/default/tomcat7

echo "+++ start usergrid"
service tomcat7 start


# database setup
# ==============

while [ -z "$(curl -s localhost:8080/status | grep '"cassandraAvailable" : true')" ] ;
do
  echo "+++ tomcat log:"
  tail -n 20 /var/log/tomcat7/catalina.out
  echo "+++ waiting for cassandra being available to usergrid"
  sleep 2
done

echo "+++ usergrid database setup"
curl --user ${ADMIN_USER}:${ADMIN_PASS} -X PUT http://localhost:8080/system/database/setup

echo "+++ usergrid database bootstrap"
curl --user ${ADMIN_USER}:${ADMIN_PASS} -X PUT http://localhost:8080/system/database/bootstrap

echo "+++ usergrid superuser setup"
curl --user ${ADMIN_USER}:${ADMIN_PASS} -X GET http://localhost:8080/system/superuser/setup

